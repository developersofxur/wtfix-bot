FROM node:18.17-alpine as builder

WORKDIR /app/

COPY package.json package-lock.json /app/
RUN npm i

COPY . /app/

RUN npm run build

FROM node:18.17-alpine as runtime

WORKDIR /app/

COPY --from=builder /app/dist dist

CMD ["node", "dist/index.js"]