import { createClient } from "redis";
import winston from "winston";
import WTFIXClient, { BotModule } from "./wtfixclient";
import { Events, GatewayIntentBits, Options } from "discord.js";
import { Client } from "pg";
import AutoChanModule from "./modules/autoChan";
import DebugModule from "./modules/debug";
import WTFIXModule from "./modules/wtfix";

/**
 * Initialize winston
 */
winston.configure({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: "log.log" }),
    ],
    format: winston.format.combine(
        winston.format.errors({ stack: true }),
        winston.format.timestamp(),
        winston.format.json(),
    ),
});
winston.level = "info";

/**
 * Setup default logger events
 */
process.on("uncaughtException", (error) => winston.log("error", error));
process.on("unhandledRejection", (reason) =>
    winston.log("error", { reason: reason }),
);

async function run() {
    /**
     * Initialize redis connection
     */
    const redisdb = createClient({
        url: "redis://redis:6379",
    });
    redisdb.on("error", (err) => winston.log("error", err));

    const redissub = redisdb.duplicate();

    const pgclient = new Client({
        host: "postgres.db",
        port: 5432,
        database: "wtfix",
        user: "wtfix_bot",
        password: process.env.POSTGRES_PASSWORD,
    });

    await Promise.all([redisdb.connect(), redissub.connect(), pgclient.connect()]);

    const botModules: BotModule[] = [AutoChanModule, DebugModule, WTFIXModule];

    /**
     * Initialize bot
     */

    const client = new WTFIXClient(
        {
            intents: [GatewayIntentBits.Guilds],
            makeCache: Options.cacheWithLimits({
                ...Options.DefaultMakeCacheSettings,
                AutoModerationRuleManager: 0,
                BaseGuildEmojiManager: 0,
                DMMessageManager: 0,
                GuildEmojiManager: 0,
                GuildMemberManager: {
                    maxSize: 0,
                    keepOverLimit: (member) =>
                        member.id === member.client.user.id,
                },
                GuildBanManager: 0,
                GuildStickerManager: 0,
                GuildForumThreadManager: 0,
                GuildInviteManager: 0,
                GuildMessageManager: 0,
                GuildScheduledEventManager: 0,
                GuildTextThreadManager: 0,
                MessageManager: 0,
                PresenceManager: 0,
                ReactionManager: 0,
                ReactionUserManager: 0,
                StageInstanceManager: 0,
                ThreadManager: 0,
                ThreadMemberManager: 0,
                UserManager: 0,
                VoiceStateManager: 0,
            }),
        },
        pgclient,
        redisdb,
        redissub,
        botModules,
    );

    client.on(Events.Debug, (m) => {
        winston.log("debug", m);
    });
    client.on(Events.Warn, (m) => {
        winston.log("warn", m);
    });
    client.on(Events.Error, (e) => {
        winston.log("error", e);
    });

    client.on(Events.GuildCreate, () => {
        winston.log("info", { label: "guild_join" });
    });
    client.on(Events.GuildDelete, () => {
        winston.log("info", { label: "guild_leave" });
    });

    client.on(Events.ShardDisconnect, (e, s) => {
        winston.log("error", {
            event: "shardDisconnect",
            shardId: s,
            error: e,
        });
    });
    client.on(Events.ShardError, (e, s) => {
        winston.log("error", { event: "shardError", shardId: s, error: e });
    });
    client.on(Events.ShardReady, (s, ug) => {
        winston.log("info", {
            event: "shardReady",
            shardId: s,
            unavailableGuilds: ug,
        });
    });
    client.on(Events.ShardReconnecting, (s) => {
        winston.log("error", { event: "shardReconnecting", shardId: s });
    });
    client.on(Events.ShardResume, (s, re) => {
        winston.log("error", {
            event: "shardResume",
            shardId: s,
            replayedEvents: re,
        });
    });

    client.login(process.env.DISCORD_TOKEN);
}

run()
    .then()
    .catch((err) => {
        winston.error(err);
        process.exit(1);
    });
