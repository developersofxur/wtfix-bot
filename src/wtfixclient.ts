import {
    ChatInputCommandInteraction,
    Client,
    ClientOptions,
    Collection,
    Guild,
    GuildTextBasedChannel,
    MessageCreateOptions,
    REST,
    RESTPostAPIChatInputApplicationCommandsJSONBody,
    Routes,
    SlashCommandBuilder,
} from "discord.js";
import winston from "winston";
import isSameVisit from "./utils/isSameVisit";
import parseXur from "./utils/parseXur";
import buildXurEmbed, { XurUpdate } from "./utils/buildXurEmbed";
import { createClient } from "redis";
import pg from "pg";
import { PostgresProvider } from "./postgres-provider";

export default class WTFIXClient extends Client {
	provider: ConfigProvider;

	private commands = new Collection<string, CommandModule>();

    doingAutoChan = false;

    constructor(
        options: ClientOptions,
        pgclient: pg.Client,
        public redisdb: ReturnType<typeof createClient>,
        public redissub: ReturnType<typeof createClient>,
        modules: BotModule[],
    ) {
        super(options);

        this.provider = new PostgresProvider(pgclient, this);
        this.initProvider().then();

        this.redissub
            .subscribe("xur", () => {
                winston.log("info", "Xur's location updated!");
                //this.xurAutoChan();
            })
            .then();

        const commands: RESTPostAPIChatInputApplicationCommandsJSONBody[] = [];
        const debugcommands: RESTPostAPIChatInputApplicationCommandsJSONBody[] =
            [];

        for (const module of modules) {
            if (module.isDebug) {
                if (module.commands) {
                    for (const command of module.commands) {
                        debugcommands.push(command.data.toJSON());
                        this.commands.set(command.data.name, command);
                    }
                }
            } else {
                if (module.commands) {
                    for (const command of module.commands) {
                        commands.push(command.data.toJSON());
                        this.commands.set(command.data.name, command);
                    }
                }
            }
            if (module.events) {
                for (const event of module.events) {
                    this.on(event.name, event.listener);
                }
            }
        }

        this.on("interactionCreate", async (interaction) => {
            if (!interaction.isChatInputCommand()) return;

            const command = this.commands.get(interaction.commandName);

            if (!command) return;

            try {
                if (!interaction.inCachedGuild()) {
                    throw new Error("Interaction is not cached!");
                }

                winston.log("info", {
                    label: "command_run",
                    command: interaction.commandName,
                });
                await command.execute(interaction);
            } catch (err) {
                winston.log("error", {
                    label: "command_error",
                    command: interaction.commandName,
                    error: err,
                });

                await interaction.reply({
                    content:
                        "An error occurred while executing that command.  Please see the #wtfix-support channel in the WTFIX discord server for help.",
                    ephemeral: true,
                });
            }
        });

        if (process.env.DISCORD_TOKEN == undefined) {
            throw "DISCORD_TOKEN not set";
        }

        const rest = new REST({ version: "10" }).setToken(
            process.env.DISCORD_TOKEN,
        );

        this.on("ready", async () => {
            winston.log(
                "info",
                `The bot is online, in ${this.guilds.cache.reduce(
                    (x) => x + 1,
                    0,
                )} servers, and services ${this.users.cache.reduce(
                    (x) => x + 1,
                    0,
                )} users!`,
            );

            this.user?.setActivity("Xur???");

            if (process.env.DISCORD_ID == undefined) {
                throw "DISCORD_ID not set";
            }

            await rest.put(Routes.applicationCommands(process.env.DISCORD_ID), {
                body: commands,
            });

            await rest.put(
                Routes.applicationGuildCommands(
                    process.env.DISCORD_ID,
                    "959611739744391188",
                ),
                { body: debugcommands },
            );
        });
    }

    async getAutoChannel(
        guildid: string,
    ): Promise<{ guild: string; id?: string; name?: string } | undefined> {
        let resp: { guild: string; id?: string; name?: string } | undefined =
            undefined;
        const guild = this.guilds.resolve(guildid);
        if (guild) {
            resp = { guild: guildid };
            const autoChanId = await this.provider.get(
                guild.id,
                "updateChan",
                null,
            );
            if (autoChanId) {
                resp.id = autoChanId;
                const channel = this.channels.resolve(
                    resp.id,
                ) as GuildTextBasedChannel;
                if (channel) {
                    resp.name = channel.name;
                }
            }
        }
        return resp;
    }

    async xurAutoChan() {
        const xurUpdate = (await this.redisdb.json.get("wtfix:xur", {
            path: ".",
        })) as XurUpdate;
        if (xurUpdate?.present && xurUpdate?.location) {
            if (this.doingAutoChan) {
                winston.info("Already doing auto chan, skipping...");
                return;
            } else {
                this.doingAutoChan = true;
            }
            let totalGuilds = 0;
            let totalAutopostGuilds = 0;
            let totalCancelledPosts = 0;
            let totalErroredPosts = 0;
            let totalSuccessfulPosts = 0;

            winston.info(
                "Is present and found, updating guild auto channels...",
            );
            for (const guild of this.guilds.cache.values()) {
                totalGuilds++;
                const updateChanId = await this.provider.get<string | null>(
                    guild.id,
                    "updateChan",
                    null,
                );
                winston.info("Servicing guild " + guild.id, {
                    autochanId: updateChanId,
                });
                if (updateChanId) {
                    totalAutopostGuilds++;
                    let chan;
                    try {
                        chan = (await this.channels.fetch(
                            updateChanId,
                        )) as GuildTextBasedChannel;
                    } catch (error) {
                        totalErroredPosts++;
                        winston.error(error);
                        continue;
                    }
                    if (chan) {
                        winston.info("Clan has auto channel");
                        const lastUpdateTimestamp = await this.provider.get<
                            number | null
                        >(guild.id, "lastUpdateTimestamp", null);
                        if (lastUpdateTimestamp) {
                            const currentTime = Date.now();
                            if (isSameVisit(lastUpdateTimestamp, currentTime)) {
                                const lastUpdateLoc = await this.provider.get<
                                    string | null
                                >(guild.id, "lastUpdateLoc", null);
                                if (lastUpdateLoc) {
                                    if (
                                        lastUpdateLoc ==
                                        parseXur(xurUpdate.location)
                                    ) {
                                        winston.log(
                                            "info",
                                            "Last update happened this same xur visit, and already posted this same location, not sending another one.",
                                        );
                                        totalCancelledPosts++;
                                        continue;
                                    }
                                } else {
                                    winston.log(
                                        "error",
                                        "Last update was this same xur visit, but xur location was also the same, this should never happen!",
                                    );
                                    totalCancelledPosts++;
                                    continue;
                                }
                            }
                        }
                        try {
                            const msgOpts: MessageCreateOptions = {
                                embeds: [buildXurEmbed(xurUpdate)],
                            };
                            if (guild.id == "494890625330577409") {
                                msgOpts["content"] = "<@&1090165057851228220>";
                            }
                            const msg = await chan.send(msgOpts);
                            await this.provider.set(
                                guild.id,
                                "lastUpdateMsg",
                                msg.id,
                            );
                            await this.provider.set(
                                guild.id,
                                "lastUpdateTimestamp",
                                Date.now(),
                            );
                            await this.provider.set(
                                guild.id,
                                "lastUpdateLoc",
                                parseXur(xurUpdate.location),
                            );
                            totalSuccessfulPosts++;
                        } catch (error) {
                            totalErroredPosts++;
                            winston.log("error", error);
                        }
                    }
                }
            }

            this.doingAutoChan = false;

            this.shard?.broadcastEval(
                (shardclient, ctx) => {
                    shardclient.channels
                        .fetch("972197313000931328")
                        .then((statusChan) => {
                            (statusChan as GuildTextBasedChannel)?.send(
                                `Auto post done on shard ${ctx.currentShard}: out of ${ctx.totalGuilds} servers this shard is in, ${ctx.totalAutopostGuilds} were registered for auto posts.  Of these auto posts, ${ctx.totalSuccessfulPosts} were successful, ${ctx.totalCancelledPosts} were cancelled, and ${ctx.totalErroredPosts} errored.`,
                            );
                        });
                },
                {
                    context: {
                        currentShard: this.shard.ids,
                        totalGuilds: totalGuilds,
                        totalAutopostGuilds: totalAutopostGuilds,
                        totalSuccessfulPosts: totalSuccessfulPosts,
                        totalCancelledPosts: totalCancelledPosts,
                        totalErroredPosts: totalErroredPosts,
                    },
                },
            );
        }
    }

    async initProvider() {
        if (this.readyTimestamp) {
            this.emit(
                "debug",
                `Provider set to ${this.provider.constructor.name} - initialising...`,
            );
            await this.provider.init();
            this.emit("debug", "Provider finished initialisation.");
            return undefined;
        }

        this.emit(
            "debug",
            `Provider set to ${this.provider.constructor.name} - will initialise once ready.`,
        );
        await new Promise((resolve) => {
            this.once("ready", () => {
                this.emit("debug", `Initialising provider...`);
                resolve(this.provider.init());
            });
        });

        this.emit("providerReady", this.provider);
        this.emit("debug", "Provider finished initialisation.");
        return undefined;
    }

    async destroy() {
        await super.destroy();
        if (this.provider) await this.provider.destroy();
    }
}

export interface ConfigProvider {
    init(): Promise<void>;
    destroy(): Promise<void>;

    get<T>(guild: string | Guild, key: string, defVal: T): Promise<T>;
    set(
        guild: string | Guild,
        key: string,
        val: string | number,
    ): Promise<void>;
    remove(guild: string | Guild, key: string): Promise<void>;

    clear(guild: string | Guild): Promise<void>;

    updateAllShards(key: string, val: string | number | undefined): void;
}

export interface BotModule {
    commands?: CommandModule[];
    events?: {
        name: string;
        listener(...args: unknown[]): Promise<void>;
    }[];
    isDebug?: boolean;
}

export interface CommandModule {
    data:
        | SlashCommandBuilder
        | Omit<SlashCommandBuilder, "addSubcommand" | "addSubcommandGroup">;
    execute(interaction: ChatInputCommandInteraction<"cached">): Promise<void>;
}
