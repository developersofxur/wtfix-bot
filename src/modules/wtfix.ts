import { SlashCommandBuilder } from "discord.js";
import WTFIXClient, { BotModule } from "../wtfixclient";
import buildXurEmbed, { XurUpdate } from "../utils/buildXurEmbed";

const WTFIXModule: BotModule = {
    commands: [{
        data: new SlashCommandBuilder()
            .setName("wtfix")
            .setDescription("Prints xur's current location in the game (or lack thereof)."),
        async execute(interaction) {
            const xur = await (interaction.client as WTFIXClient).redisdb.json.get("wtfix:xur", {
                path: "."
            }) as XurUpdate;
            if (!xur) {
                throw new Error("Xur not found in redisdb!");
            }
            await interaction.reply({
                embeds: [buildXurEmbed(xur)]
            })
        }
    }]
}

export default WTFIXModule