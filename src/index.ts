/* eslint-disable @typescript-eslint/no-var-requires */

const { ShardingManager } = require('discord.js');
const winston = require('winston');

import type { Shard } from 'discord.js';

winston.configure({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'log.log' }),
    ],
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint()
    ),
});
winston.level = 'debug';

const manager = new ShardingManager(__dirname + '/bot.js', { token: process.env.DISCORD_TOKEN });

process.on('uncaughtException', error => winston.log('error', error));
process.on('unhandledRejection', (reason) => winston.log('error', {reason: reason}));

manager.on('shardCreate', (shard: Shard) => {() => winston.log('info', `Launched shard ${shard.id}`)});
manager.spawn();